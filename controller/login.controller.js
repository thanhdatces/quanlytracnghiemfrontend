var request = require('request');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');
const Url = require('../public/url');

exports.login = function (req, res) {
    var data = {
        username: req.body.username,
        password: req.body.pass,
    };
    request.post(
        Url.url() + "/account/checkLogin",{json: data}, function(error, response, body) {
            if(error){
                res.render('error',{
                    message: error.errno,
                    error: error
                });
            }
            else{
                console.log(body.data);
                if(body.token){
                    localStorage.setItem('token', body.token);
                    localStorage.setItem('_id', body.data._id);
                    localStorage.setItem('username', body.data.UserName);
                }
                res.render('index',{
                    title: 'Express',
                    action: 'login',
                    username: localStorage.getItem('username'),
                    mes: body.message,
                    status: body.status,
                });
            }
        });
};

exports.register = function (req, res) {
    var data = {
        username: req.body.username,
        password: req.body.pass,
        facebook: req.body.facebook,
        gmail: req.body.gmail,
        idrole: req.body.role,
    };
    request.post(
        Url.url() + "/account/create",{json: data}, function(error, response, body) {
            if(error){
                res.render('error',{
                    message: error.errno,
                    error: error
                });
            }
            else{
                res.render('index',{
                    title: 'Express',
                    action: 'register',
                    mes: body.message,
                    status: body.status,
                });
            }
        });
};

exports.logout = function (req, res) {
    res.render('index', {
        title: 'Express',
        action: 'logout',
    });
    localStorage.clear();
};
