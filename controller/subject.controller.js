var request = require('request');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');
const Url = require('../public/url');

exports.getAllSubjectByIdAccount = function (req, res) {
    request({headers: {
            'Authorization': 'Bearer '+ localStorage.getItem('token')
        },
        url: Url.url() + '/class/idAccount/' + localStorage.getItem('_id'),
        method: 'GET'
    }, function(error, response, body) {
        if(error){
            res.render('error',{
                message: error.errno,
                error: error
            });
        }
        else{
            var json = JSON.parse(body);
            if(!json.status){
                res.render('error', {
                    message: json.message,
                    error: json.data,
                });
            }
            else{
                res.render('subject', {
                    mes: json.message,
                    status: json.status,
                    subject: json.data,
                    username: localStorage.getItem('username'),
                });
            }
        }
    });
};
