var request = require('request');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');
const Url = require('../public/url');

exports.getAllQuestionBySubject = function (req, res) {
    request({headers: {
            'Authorization': 'Bearer '+ localStorage.getItem('token')
        },
        url: Url.url() + '/question/subject',
        qs: {
            'idSubject': req.params.id
        },
        method: 'GET'
    }, function(error, response, body) {
        if(error){
            res.render('error',{
                message: error.errno,
                error: error
            });
        }
        else{
            var json = JSON.parse(body);
            if(!json.status){
                res.render('error', {
                    message: json.message,
                    error: json.data,
                });
            }
            else{
                res.render('question', {
                    mes: json.message,
                    status: json.status,
                    question: json.data,
                    username: localStorage.getItem('username'),
                });
            }
        }
    });
};

exports.createQuestion = function (req, res) {
    var data = {
        question: req.body.question,
        idsubject: req.body.idsubject,
        answer: [{ content: req.body.a1},
            { content: req.body.a2},
            {content: req.body.a3},
            {content: req.body.a4}]
    };
    request.post(
        "http://localhost:1234/question/create",{json: data}, function(error, response, body) {
            if(error){
                res.render('error',{
                    message: error.errno,
                    error: error
                });
            }
            else{
                res.render('question',{
                    question: '',
                    mes: body,
                    username: localStorage.getItem('username'),
                })
            }
        });
};
