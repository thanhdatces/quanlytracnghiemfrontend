var express = require('express');
var router = express.Router();
var request = require('request');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');
const Url = require('../public/url');

router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express',
        username: localStorage.getItem('username'),
        status: ''
    });
});
router.post('/test', function(req, res) {
    var data = {
        idtestdetail: req.body.idtestdetail,
        idclass: req.body.idclass,
    };
    request({
        headers: {
                'Authorization': 'Bearer '+ localStorage.getItem('token')
            },
        url: Url.url() + "/test/begin",
        json: data,
        method: 'POST'
        }, function(error, response, body) {
            if(error){
                res.render('error',{
                    message: error.errno,
                    error: error
                });
            }
            else{
                res.render('test',{
                    questions: body.data,
                    mes: 'success'
                });
                console.log(body)
            }
        });
});
module.exports = router;
