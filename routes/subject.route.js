var express = require('express');
var router = express.Router();
const subjectcontroller = require('../controller/subject.controller');

/* GET home page. */
router.get('/', subjectcontroller.getAllSubjectByIdAccount);
module.exports = router;
