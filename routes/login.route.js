var express = require('express');
var router = express.Router();
const loginContoller = require('../controller/login.controller');

router.post('/', function(req, res) {
    if(req.body.action == 'loginRoute'){
        loginContoller.login(req, res);
    }
    else if (req.body.action == 'register'){
        loginContoller.register(req, res);

    }
});
router.get('/logout', function (req, res) {
    loginContoller.logout(req, res);
});
module.exports = router;
