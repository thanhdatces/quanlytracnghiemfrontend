var express = require('express');
var router = express.Router();
const questionController = require('../controller/question.controller');

/* GET home page. */
router.get('/subject/:id', questionController.getAllQuestionBySubject);
router.post('/create', questionController.createQuestion);
module.exports = router;
