var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var question = require('./routes/question.route');
var subject = require('./routes/subject.route');
var test = require('./routes/test');
var account = require('./routes/login.route');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


function redirectUnmatched(route) {
  if (localStorage.getItem('token')!=null) return route;
  else return function (req, res) {
      res.render('error',{
          message: 'You have not logged in yet',
          error: 'Please Log in'
      });
  }
}

app.use('/', redirectUnmatched(indexRouter));
app.use('/users', redirectUnmatched(usersRouter));
app.use('/question', redirectUnmatched(question));
app.use('/subject', redirectUnmatched(subject));
app.use('/test', redirectUnmatched(test));
app.use('/', redirectUnmatched(account));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
